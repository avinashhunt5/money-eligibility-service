package com.avinash.moneyeligibility.steps;

import com.avinash.moneyeligibility.chainofresponsibility.Commander;
import com.avinash.moneyeligibility.model.EligibilityEsbResponse;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java8.En;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

public class MoneyEligibilityStepdefs implements En {

    @Autowired
    @Qualifier("moneyEligibility")
    private Commander<EligibilityRequest, EligibilityResponse> commander;

    private EligibilityRequest request;
    private EligibilityResponse response = new EligibilityResponse();

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    public MoneyEligibilityStepdefs() {

        Given("^the person annual salary as (\\d+)$", (Integer salary) -> {
            request = new EligibilityRequest();
            request.setSalary(salary);
            mockServer = MockRestServiceServer.createServer(restTemplate);
        });

        And("^the gold assets of (\\d+)$", (Integer goldAssets) -> {
            request.setGoldAsset(goldAssets);
        });

        When("^the money eligibility checked$", () -> {
            EligibilityEsbResponse eligibilityEsbResponse = new EligibilityEsbResponse();
            eligibilityEsbResponse.setMessage("SUCCESS");
            mockServer.expect(ExpectedCount.once(),
                    requestTo(new URI("http://localhost:9898/money-eligibility-esb/")))
                    .andExpect(method(HttpMethod.POST))
                    .andRespond(withStatus(HttpStatus.OK)
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(mapper.writeValueAsString(eligibilityEsbResponse))
                    );
            commander.processCommand(request, response);
        });

        Then("^\"([^\"]*)\" should be matched$", (String eligibilityResponse) -> {
            String message = response.getMessage();
            Assert.assertEquals("Person is eligibility matches ", eligibilityResponse, message);
        });
    }
}
