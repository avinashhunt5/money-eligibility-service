package com.avinash.moneyeligibility;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:build/cucumber-report.html"},
        features = {"src/test/resources/features"}
)
public class RunCucumberTest {
}
