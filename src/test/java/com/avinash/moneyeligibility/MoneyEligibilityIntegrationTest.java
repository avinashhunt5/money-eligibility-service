package com.avinash.moneyeligibility;

import io.cucumber.spring.CucumberContextConfiguration;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@ContextConfiguration(classes = MoneyEligibilityServiceApplication.class, loader = SpringBootContextLoader.class)
@CucumberContextConfiguration
public class MoneyEligibilityIntegrationTest {
}