Feature: Eligibility Check

  @Eligibility
  Scenario: Eligibility Check for Person based on salary and gold assets should be eligible
    Given the person annual salary as 250000
    And the gold assets of 50000
    When the money eligibility checked
    Then "RetailCustomer Customer handling done" should be matched

  @NotEligibility
  Scenario: Eligibility Check for Person based on salary and gold assets should not be eligible
    Given the person annual salary as 5000
    And the gold assets of 50000
    When the money eligibility checked
    Then "Salary is less than Rs 10000" should be matched