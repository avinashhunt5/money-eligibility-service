package com.avinash.moneyeligibility.controller;

import com.avinash.moneyeligibility.model.UserModel;
import com.avinash.moneyeligibility.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/registerUser")
    public UserModel addUser(@RequestBody UserModel employee) {
        return userRepository.addUser(employee);
    }

    @GetMapping("/getUser")
    public ResponseEntity<UserModel> isUserValidated(@RequestParam(value = "userId", required = true) String userId) {

        UserModel user = null;
        user = userRepository.getUser(userId);
        if(null != user) {
           logger.info("User exist !!");
        } else {
            logger.info("User {} no found", userId);
        }
        return ResponseEntity.ok(user);
    }
}
