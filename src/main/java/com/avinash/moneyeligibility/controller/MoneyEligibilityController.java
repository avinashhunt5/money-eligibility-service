package com.avinash.moneyeligibility.controller;

import com.avinash.moneyeligibility.chainofresponsibility.Commander;
import com.avinash.moneyeligibility.exception.InvalidRequestException;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/eligibility")
public class MoneyEligibilityController {

    private final Logger logger = LoggerFactory.getLogger(MoneyEligibilityController.class);

    @Autowired
    @Qualifier("moneyEligibility")
    private Commander<EligibilityRequest, EligibilityResponse> commander;

    @PostMapping("/isEligible")
    ResponseEntity<EligibilityResponse> isEligible(@RequestBody EligibilityRequest request) {
        EligibilityResponse response = new EligibilityResponse();
        try {
            logger.info("request reached for Money eligibility check !!!");
            commander.processCommand(request, response);
        } catch (InvalidRequestException exception) {
            logger.warn("bad request for money check !!!");
            response.setEligible(false);
            response.setMessage(exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } catch (Exception exception) {
            logger.error("exception occurs in isEligible{} ", exception.getMessage());
            response.setEligible(false);
            response.setMessage(exception.getMessage());
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
