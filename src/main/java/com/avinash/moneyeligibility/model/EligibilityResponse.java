package com.avinash.moneyeligibility.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EligibilityResponse {

    private String message;
    private boolean isEligible;

    @JsonIgnore
    private boolean salaryCheck;

    @JsonIgnore
    private boolean goldCheck;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isEligible() {
        return isEligible;
    }

    public void setEligible(boolean eligible) {
        isEligible = eligible;
    }

    public boolean isSalaryCheck() {
        return salaryCheck;
    }

    public void setSalaryCheck(boolean salaryCheck) {
        this.salaryCheck = salaryCheck;
    }

    public boolean isGoldCheck() {
        return goldCheck;
    }

    public void setGoldCheck(boolean goldCheck) {
        this.goldCheck = goldCheck;
    }
}
