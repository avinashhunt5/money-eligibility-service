package com.avinash.moneyeligibility.model;

public class EligibilityRequest {

    private double salary;
    private boolean loanTaken;
    private double goldAsset;
    private double previousLoanAmt;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public boolean isLoanTaken() {
        return loanTaken;
    }

    public void setLoanTaken(boolean loanTaken) {
        this.loanTaken = loanTaken;
    }

    public double getGoldAsset() {
        return goldAsset;
    }

    public void setGoldAsset(double goldAsset) {
        this.goldAsset = goldAsset;
    }

    public double getPreviousLoanAmt() {
        return previousLoanAmt;
    }

    public void setPreviousLoanAmt(double previousLoanAmt) {
        this.previousLoanAmt = previousLoanAmt;
    }
}
