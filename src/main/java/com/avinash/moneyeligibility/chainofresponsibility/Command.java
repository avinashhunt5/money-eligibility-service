package com.avinash.moneyeligibility.chainofresponsibility;

public interface Command<R,S> {

    default void execute(R r) {
    }

    default void process(R r, S s){

    }

    default boolean isProcessor() {
        return false;
    }
}
