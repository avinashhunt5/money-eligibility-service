package com.avinash.moneyeligibility.chainofresponsibility;

public interface Commander<R,S> extends Command<R,S> {
    void processCommand(R r, S s);
}
