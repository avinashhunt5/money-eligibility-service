package com.avinash.moneyeligibility.chainofresponsibility;

import java.util.Arrays;

public class AbstractCommander<R,S> implements Commander<R,S> {

    private Command<R,S> [] commands;

    @Override
    public void processCommand(R r, S s) {
        for(Command<R,S> command : commands) {
            if(command.isProcessor()) {
                command.process(r,s);
            } else {
                command.execute(r);
            }
        }
    }

    public void setCommands(Command<R,S> [] commands) {
        this.commands = Arrays.copyOf(commands,commands.length);
    }
}
