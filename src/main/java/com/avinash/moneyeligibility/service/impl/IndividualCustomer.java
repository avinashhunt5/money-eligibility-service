package com.avinash.moneyeligibility.service.impl;

import com.avinash.moneyeligibility.service.ISalaryStrategy;
import org.springframework.stereotype.Component;

@Component
public class IndividualCustomer implements ISalaryStrategy {

    @Override
    public String process(double salary) {
        return "IndividualCustomer Customer handling done";
    }
    //Salary should be within 10000 to 100000
    @Override
    public boolean salaryType(double salary) {
        boolean res = false;
        if ((Double.compare(salary, 10000) > 0) &&
                (Double.compare(salary, 100000) < 0)) {
            res = true;
        }
        return res;
    }
}
