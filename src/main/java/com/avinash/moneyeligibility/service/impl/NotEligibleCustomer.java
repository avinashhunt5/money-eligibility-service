package com.avinash.moneyeligibility.service.impl;

import com.avinash.moneyeligibility.service.ISalaryStrategy;
import org.springframework.stereotype.Component;

@Component
public class NotEligibleCustomer implements ISalaryStrategy {

    @Override
    public String process(double salary) {
        return "Salary is less than Rs 10000";
    }
    //Salary should be within 0 to 10000
    @Override
    public boolean salaryType(double salary) {
        boolean res = false;
        if ((Double.compare(salary, 0) > 0) &&
                (Double.compare(salary, 10000) < 0)) {
            res = true;
        }
        return res;
    }
}
