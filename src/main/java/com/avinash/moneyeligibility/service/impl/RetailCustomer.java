package com.avinash.moneyeligibility.service.impl;

import com.avinash.moneyeligibility.service.ISalaryStrategy;
import org.springframework.stereotype.Component;

@Component
public class RetailCustomer implements ISalaryStrategy {

    @Override
    public String process(double salary) {
        return "RetailCustomer Customer handling done";
    }
    //Salary should be within 100000 to 500000
    @Override
    public boolean salaryType(double salary) {
        boolean res = false;
        if ((Double.compare(salary, 100000) > 0) &&
                (Double.compare(salary, 500000) < 0)) {
            res = true;
        }
        return res;
    }
}
