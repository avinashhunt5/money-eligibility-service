package com.avinash.moneyeligibility.service.impl;

import com.avinash.moneyeligibility.service.ISalaryStrategy;
import org.springframework.stereotype.Component;

@Component
public class PremiumCustomer implements ISalaryStrategy {

    @Override
    public String process(double salary) {
        return "Premium Customer handling done";
    }
    //Salary should be within 500000 to 1000000
    @Override
    public boolean salaryType(double salary) {
        boolean res = false;
        if ((Double.compare(salary, 500000) > 0) &&
                (Double.compare(salary, 1000000) < 0)) {
            res = true;
        }
        return res;
    }
}
