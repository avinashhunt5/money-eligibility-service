package com.avinash.moneyeligibility.service;

public interface ISalaryStrategy {

    String process(double salary);
    boolean salaryType(double salary);

}
