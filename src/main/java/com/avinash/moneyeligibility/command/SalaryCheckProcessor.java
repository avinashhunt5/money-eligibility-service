package com.avinash.moneyeligibility.command;

import com.avinash.moneyeligibility.chainofresponsibility.Command;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;
import com.avinash.moneyeligibility.service.ISalaryStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Optional;

public class SalaryCheckProcessor implements Command<EligibilityRequest, EligibilityResponse> {

    @Autowired
    private List<ISalaryStrategy> salaryStrategyList;

    @Override
    public void process(EligibilityRequest eligibilityRequest, EligibilityResponse response) {
        response.setMessage("Eligible for Loan");
        response.setEligible(true);
        Optional<ISalaryStrategy> salaryStrategy = salaryStrategyList.stream()
                .filter(salaryStrategyList -> salaryStrategyList.salaryType(eligibilityRequest.getSalary()))
                .findAny();
        if (salaryStrategy.isPresent()) {
            response.setEligible(false);
            response.setMessage(salaryStrategy.get().process(eligibilityRequest.getSalary()));
        }
    }

    @Override
    public boolean isProcessor() {
        return true;
    }
}
