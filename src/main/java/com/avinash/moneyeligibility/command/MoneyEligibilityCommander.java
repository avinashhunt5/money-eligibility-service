package com.avinash.moneyeligibility.command;

import com.avinash.moneyeligibility.chainofresponsibility.AbstractCommander;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;

public class MoneyEligibilityCommander extends AbstractCommander<EligibilityRequest, EligibilityResponse> {
}
