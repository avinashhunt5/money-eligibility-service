package com.avinash.moneyeligibility.command;

import com.avinash.moneyeligibility.chainofresponsibility.Command;
import com.avinash.moneyeligibility.exception.InvalidRequestException;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;

public class MoneyEligibilityRequestValidator implements Command<EligibilityRequest, EligibilityResponse> {

    @Override
    public void execute(EligibilityRequest eligibilityRequest) {
        if (Double.isNaN(eligibilityRequest.getSalary())) {
            throw new InvalidRequestException("Salary is required");
        }
    }
}
