package com.avinash.moneyeligibility.command;

import com.avinash.moneyeligibility.chainofresponsibility.Command;
import com.avinash.moneyeligibility.model.EligibilityEsbResponse;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GoldEligibilityCheckProcessor implements Command<EligibilityRequest, EligibilityResponse> {

    @Value("${money.eligibility.esb.service.url}")
    private String esbUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void process(EligibilityRequest eligibilityRequest, EligibilityResponse response) {
        EligibilityRequest esbRequest = new EligibilityRequest();
        esbRequest.setSalary(eligibilityRequest.getSalary());
        esbRequest.setGoldAsset(eligibilityRequest.getGoldAsset());
        ResponseEntity<EligibilityEsbResponse> eligibilityEsbResponse =
                restTemplate.postForEntity(esbUrl, esbRequest, EligibilityEsbResponse.class);
        if (eligibilityEsbResponse.getStatusCode().is2xxSuccessful() && eligibilityEsbResponse.getBody()
                .getMessage().equalsIgnoreCase("SUCCESS")) {
            response.setGoldCheck(true);
        } else {
            response.setGoldCheck(false);
            StringBuilder builder = new StringBuilder("Gold Message :");
            builder.append(eligibilityEsbResponse.getBody().getGoldAssetServiceResponse());
            builder.append(" Kafka Service Message : ");
            builder.append(eligibilityEsbResponse.getBody().getKafkaServiceResponse());
            response.setMessage(builder.toString());
        }
    }

    @Override
    public boolean isProcessor() {
        return true;
    }
}
