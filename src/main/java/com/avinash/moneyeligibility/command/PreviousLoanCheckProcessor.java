package com.avinash.moneyeligibility.command;

import com.avinash.moneyeligibility.chainofresponsibility.Command;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;

import java.math.BigDecimal;

public class PreviousLoanCheckProcessor implements Command<EligibilityRequest, EligibilityResponse> {

    @Override
    public void process(EligibilityRequest eligibilityRequest, EligibilityResponse response) {
        if (eligibilityRequest.isLoanTaken() &&
                BigDecimal.valueOf(eligibilityRequest.getPreviousLoanAmt()).compareTo(BigDecimal.valueOf(50000)) < 0) {
            response.setEligible(false);
            response.setMessage("Previous loan taken with amount more than 50000");
        }
    }

    @Override
    public boolean isProcessor() {
        return true;
    }
}
