package com.avinash.moneyeligibility.config;

import com.avinash.moneyeligibility.chainofresponsibility.Command;
import com.avinash.moneyeligibility.command.*;
import com.avinash.moneyeligibility.model.EligibilityRequest;
import com.avinash.moneyeligibility.model.EligibilityResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MoneyEligibilityConfig {

    @Bean("moneyEligibility")
      public MoneyEligibilityCommander moneyEligibilityCommander() {

          MoneyEligibilityCommander moneyEligibilityCommander = new MoneyEligibilityCommander();

          Command<EligibilityRequest, EligibilityResponse> command[] = new Command[] {
                  moneyEligibilityRequestValidator(),
                  salaryCheckProcessor(),
                  goldEligibilityCheckProcessor(),
                  previousLoanCheckProcessor()
          };

          moneyEligibilityCommander.setCommands(command);
          return moneyEligibilityCommander;
      }

      @Bean
      public MoneyEligibilityRequestValidator moneyEligibilityRequestValidator() {
          return new MoneyEligibilityRequestValidator();
      }

      @Bean
      public SalaryCheckProcessor salaryCheckProcessor() {
          return new SalaryCheckProcessor();
      }

      @Bean
      public GoldEligibilityCheckProcessor goldEligibilityCheckProcessor() {
          return new GoldEligibilityCheckProcessor();
      }

      @Bean
      public PreviousLoanCheckProcessor previousLoanCheckProcessor() {
          return new PreviousLoanCheckProcessor();
      }

      @Bean
      public RestTemplate restTemplate() {
        return new RestTemplate();
      }
}
