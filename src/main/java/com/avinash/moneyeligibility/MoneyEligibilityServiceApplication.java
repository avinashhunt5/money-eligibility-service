package com.avinash.moneyeligibility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyEligibilityServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoneyEligibilityServiceApplication.class, args);
	}

}
