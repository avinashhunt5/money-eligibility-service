package com.avinash.moneyeligibility.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.avinash.moneyeligibility.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public UserModel addUser(UserModel user) {
         dynamoDBMapper.save(user);
         return user;
    }

    public UserModel getUser(String userId) {
        UserModel userModel = new UserModel();
        userModel.setFirstName("Avinash");
        userModel.setUserId("avinashit");
        userModel.setLastName("Kumar");
        userModel.setEmailId("avinash.it2009@gmail.com");
        userModel.setPassword("March@1234");
        return userModel;//dynamoDBMapper.load(UserModel.class, userId);
    }

}
