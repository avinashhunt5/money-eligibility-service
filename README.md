# Money Eligibility Service

## Steps to BUILD and TEST LOCAL
1. build the project using gradle clean build
2. get the JWT token first by trigger POST request with
   http://localhost:8083/money-eligibility-service/authenticate
   Request JSON : 
   {
   "username": "avinashit",
   "password": "March@1234"
   }
3. trigger the POST request with JWT token generated from step 2
   http://localhost:8083/money-eligibility-service/eligibility/isEligible
   Request JSON :
   {
   "salary" : "2000",
   "goldAsset": "35000"
   }

## Steps to BUILD and RUN
1. Goto Docker folder under this repo and run 
docker build .
Make sure the docker is running locally
